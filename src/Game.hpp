#pragma once
#include "Library.hpp"
#include "gfx/Screen.hpp"

class Game {
    //private Random random = new Random();
    public static const wstring NAME;
    public static const int HEIGHT;
    public static const int WIDTH;
    private static const int SCALE;

    //private BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
    //private int[] pixels = ((DataBufferInt)image.getRaster().getDataBuffer()).getData();
    private bool running = false;
    private unique_ptr<Screen> screen;
    private unique_ptr<Screen> lightScreen;
    //private InputHandler input = new InputHandler(this);

    //private int[] colors = new int[256];
    private int tickCount = 0;
    //public int gameTime = 0;

    //private Level level;
    //private Level[] levels = new Level[5];
    //private int currentLevel = 3;
    //public Player player;

    //public Menu menu;
    //private int playerDeadTime;
    //private int pendingLevelChange;
    //private int wonTimer = 0;
    //public boolean hasWon = false;

    public Game();
    //public void SetMenu(Menu menu);
    public void Start();
    public void Stop();
    //public void ResetGame();
    private void Init();
    public void Run();
    public void Tick();
    //public void ChangeLevel(int dir);
    public void Render();
    //private void RenderGui();
    //private void RenderFocusNagger();
    //public void ScheduleLevelChange(int dir);
    public static void Main();
    //public void Won();
};
