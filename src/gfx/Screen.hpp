#pragma once
#include "../Library.hpp"

class Screen {
    public int xOffset;
    public int yOffset;

    public static const int BIT_MIRROR_X;
    public static const int BIT_MIRROR_Y;

    public const int w, h;
    public vector<int> pixels;

    //private SpriteSheet sheet;
    private array<int, 16> dither = { 0, 8, 2, 10, 12, 4, 14, 6, 3, 11, 1, 9, 15, 7, 13, 5 };

    public Screen(int w, int h/*, SpriteSheet sheet*/);
    public void Clear(int color);
    public void Render(int xp, int yp, int tile, int colors, int bits);
    public void SetOffset(int xOffset, int yOffset);
    public void Overlay(const Screen& screen2, int xa, int ya);
    public void RenderLight(int x, int y, int r);
};
