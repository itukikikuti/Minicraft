#pragma once
#include "../Library.hpp"
#include "../Game.hpp"
#include "../gfx/Screen.hpp"
#include "ListItem.hpp"

class Menu {
    protected const Game* game;

    public Menu();
    public void Init(const Game& game);
    public void Tick();
    public void Render(const Screen& screen);
    public void RenderItemList(const Screen& screen, int xo, int yo, int x1, int y1, const vector<ListItem>& listItems, int selected);
};
