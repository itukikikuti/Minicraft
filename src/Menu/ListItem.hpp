#include "../Library.hpp"
#include "../gfx/Screen.hpp"

class ListItem {
    public virtual void RenderInventory(Screen screen, int i, int j) = 0;
    public virtual ~ListItem() {
    }
};
