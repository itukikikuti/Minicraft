#include "Menu.hpp"

Menu::Menu() {
}

void Menu::Init(const Game& game) {
    this->game = &game;
}

void Menu::Tick() {
}

void Menu::Render(const Screen& screen) {
}

void Menu::RenderItemList(const Screen& screen, int xo, int yo, int x1, int y1, const vector<ListItem>& listItems, int selected) {
    bool renderCursor = true;
    if (selected < 0) {
        selected = -selected - 1;
        renderCursor = false;
    }
    int w = x1 - xo;
    int h = y1 - yo - 1;
    int i0 = 0;
    int i1 = listItems.size();
    if (i1 > h) i1 = h;
    int io = selected - h / 2;
    if (io > listItems.size() - h) io = listItems.size() - h;
    if (io < 0) io = 0;

    for (int i = i0; i < i1; i++) {
        listItems[i + io].RenderInventory(screen, (1 + xo) * 8, (i + 1 + yo) * 8);
    }

    if (renderCursor) {
        int yy = selected + 1 - io + yo;
        //Font.draw(">", screen, (xo + 0) * 8, yy * 8, Color.get(5, 555, 555, 555));
        //Font.draw("<", screen, (xo + w) * 8, yy * 8, Color.get(5, 555, 555, 555));
    }
}
